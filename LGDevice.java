package dathtanim.tanawat.lab5;

/*
 * LGDevice
 * 
 * 
 * @author Tanawat Dathtanim
 * @version 1.0
 * 
 */
public class LGDevice extends MobileDevice {

	private static String brand = "LG";
	private String androidVersion = "";

	public LGDevice(String modelName, String os, double price, double rating, double weight) {
		super(modelName, os, price, rating, weight);

	}

	public LGDevice(String modelName, String os, int price, double rating, int weight, String androidVersion) {
		super(modelName, os, price, rating, weight);
		this.androidVersion = androidVersion;
	}

	public static String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		LGDevice.brand = brand;
	}

	public String getAndroidVersion() {
		return androidVersion;
	}

	public void setAndroidVersion(String androidVersion) {
		this.androidVersion = androidVersion;
	}

	@Override
	public void review() {

		System.out.println("Good Android Phone");
	}

}
