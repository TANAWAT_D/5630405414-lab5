package dathtanim.tanawat.lab5;

/*
 * InterestedMobileDevices
 * 
 * 
 * @author Tanawat Dathtanim
 * @version 1.0
 * 
 */
public class InterestedMobileDevices {

	public static void main(String[] args) {

		MobileDevice g3 = new MobileDevice("LG G3", "Android", 16900, 9.5, 149);
		MobileDevice ipadmini3 = new MobileDevice("Apple iPad Mini 3", "iOS", 13400, 8, 312);
		System.out.println(g3);
		System.out.println(ipadmini3);
		g3.setRating(9.2);
		System.out.println(g3.getModelName() + " has new rating as " + g3.getRating());
		System.out.println(ipadmini3.getModelName() + " has " + " weight as " + ipadmini3.getWeight());
		MicrosoftDevice win8 = new MicrosoftDevice("Nokia Lumia", "Window Phone", 20000, 9.10, 150);
		win8.setPrice(15000);
		System.out.println(win8);

	}

};
