package dathtanim.tanawat.lab5;

/*
 * LighterDevices
 * 
 * 
 * @author Tanawat Dathtanim
 * @version 1.0
 * 
 */
public class LighterDevices {

	public static void main(String[] args) {

		MobileDevice device1 = new MicrosoftDevice("Nokia Lumia", "Window Phone", 20000, 9.0, 150, "windows 10 mobile");
		MobileDevice device2 = new LGDevice("LG G2 Mini", "Android", 8900, 4.5, 121, "Android 4.4 (KitKat)");
		MobileDevice device3 = new MicrosoftDevice("HTC One", "Window Phone", 22000, 9.5, 150, "window 10 mobile");
		MobileDevice device4 = new AppleDevice("Apple iPhone6", "iOS", 24900, 9, 129, "iOS8");
		isLighter(device1, device2);
		isLighter(device1, device3);
		isLighter(device4, device1);
		isLightest(device1, device2, device3);
		isLightest(device1, device3, device4);

	}

	public static void isLighter(MobileDevice m1, MobileDevice m2) {

		if (m1.getWeight() > m2.getWeight()) {
			System.out.println(m1.getModelName() + " is NOT lighter than " + m2.getModelName());
		} else if (m1.getWeight() == m2.getWeight()) {
			System.out.println(m1.getModelName() + " is NOT lighter than " + m2.getModelName());
		} else if (m2.getWeight() > m1.getWeight()) {
			System.out.println(m1.getModelName() + " is lighter than " + m2.getModelName());
		}
	}

	public static void isLightest(MobileDevice m1, MobileDevice m2, MobileDevice m3) {
		if (m1.getWeight() < m2.getWeight()) {

			if (m1.getWeight() < m3.getWeight()) {
				System.out.println(m1.getModelName() + " is the lightest among " + m1.getModelName() + ", "
						+ m2.getModelName() + " and " + m3.getModelName());
			}
		} else if (m2.getWeight() < m1.getWeight()) {

			if (m2.getWeight() < m3.getWeight()) {
				System.out.println(m2.getModelName() + " is the lightest among " + m1.getModelName() + ", "
						+ m2.getModelName() + " and " + m3.getModelName());
			}
		} else if (m3.getWeight() < m1.getWeight()) {

			if (m3.getWeight() < m2.getWeight()) {
				System.out.println(m3.getModelName() + " is the lightest among " + m1.getModelName() + ", "
						+ m2.getModelName() + " and " + m3.getModelName());
			}
		}
	}

}
