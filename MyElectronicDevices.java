package dathtanim.tanawat.lab5;

/*
 * Interfaces and Abstract
 * 
 * 
 * @author Tanawat Dathtanim
 * @version 1.0
 * 
 */
abstract class ElectronicDevice {
	String name;

	public ElectronicDevice(String name) {
		this.name = name;
	}

	abstract public void acceptInput();
}

class MDevice extends ElectronicDevice implements HasSensors, HasKeyboard {
	public MDevice(String name) {
		super(name);

	}

	public void acceptInput() {
		System.out.println("===name:" + name + " This is acceptInput method in class MDevice");
	}

	@Override
	public void type() {

		System.out.println("===name:" + name + " This is type method in class MDevice");
	}

	@Override
	public void accessLightSensor() {

		System.out.println("===name:" + name + " This is accessLightSensor method in class MDevice ===");
	}
}

class Desktop extends ElectronicDevice implements HasKeyboard {
	public Desktop(String name) {
		super(name);

	}

	public void acceptInput() {
		System.out.println("===name:" + name + " This is acceptInput method in class Desktop");
	}

	@Override
	public void type() {

		System.out.println("===name:" + name + " This is type method in class Desktop");
	}

}

interface HasKeyboard {
	public void type();
}

interface HasSensors {
	public void accessLightSensor();
}

public class MyElectronicDevices {

	public static void main(String[] args) {

		MDevice mdevice1 = new MDevice("mdevice1");
		Desktop desktop1 = new Desktop("desktop1");
		ElectronicDevice desktop2 = new Desktop("desktop2");
		mdevice1.acceptInput();
		mdevice1.accessLightSensor();
		mdevice1.type();
		System.out.println("++++++++++++++++++++++");
		desktop1.acceptInput();
		desktop1.type();
		desktop2.acceptInput();

	}

}
