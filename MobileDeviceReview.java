package dathtanim.tanawat.lab5;

/*
 * MobileDeviceReview
 * 
 * 
 * @author Tanawat Dathtanim
 * @version 1.0
 * 
 */
public class MobileDeviceReview {

	public static void main(String[] args) {

		MobileDevice device1 = new MicrosoftDevice("Nokia Lumia", "Window Phone", 20000, 9.0, 150, "windows 10 mobile");
		MobileDevice device2 = new LGDevice("LG G2 Mini", "Android", 8900, 4.5, 121, "Android 4.4 (KitKat)");
		MobileDevice device3 = new MicrosoftDevice("HTC One", "Window Phone", 22000, 9.5, 155, "window 10 mobile");
		MobileDevice device4 = new AppleDevice("Apple iPhone6", "iOS", 24900, 9, 129, "iOS8");
		System.out.print(device1.getModelName() + " is ");	device1.review();
		System.out.print(device2.getModelName() + " is ");	device2.review();
		device3.review("This HTC phone is a windows mobile phone");
		device1 = device2;
		System.out.print(device1.getModelName() + " is ");	device1.review();
		System.out.print(device4.getModelName() + " is ");	device4.review();

	}

}
