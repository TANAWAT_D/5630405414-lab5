package dathtanim.tanawat.lab5;

/*
 * MicrosoftDevice
 * 
 * 
 * @author Tanawat Dathtanim
 * @version 1.0
 * 
 */
public class MicrosoftDevice extends MobileDevice {

	private static String brand = "Window Phone";
	private String windowVersion = "";
	// private String windowPhoneVersion = "";

	public MicrosoftDevice(String modelName, String os, double price, double rating, double weight) {
		super(modelName, os, price, rating, weight);

	}

	public MicrosoftDevice(String modelName, String os, double price, double rating, double weight, String version) {
		super(modelName, os, price, rating, weight);
		this.windowVersion = version;
	}

	/*
	 * public static String getBrand() { return brand; }
	 * 
	 * public void setBrand(String brand) { MicrosoftDevice.brand = brand; }
	 * 
	 * public String getAndroidVersion() { return windowVersion; }
	 * 
	 * public void setAndroidVersion(String androidVersion) { this.windowVersion
	 * = androidVersion; }
	 */
	@Override
	public void review() {

		System.out.println("Good Windows Phone");
	}

	@Override
	public String toString() {
		String text = super.toString();
		if (!windowVersion.equals("")) {
			text += windowVersion;
		}
		return text;
	}
}
