package dathtanim.tanawat.lab5;

/*
 * MobileDevice
 * 
 * 
 * @author Tanawat Dathtanim
 * @version 1.0
 * 
 */
public class MobileDevice extends dathtanim.tanawat.lab4.MobileDevice {

	public MobileDevice(String modelName, String os, double price, double rating, double weight) {
		super(modelName, os, price, rating, weight);

	}

	public void review() {

		System.out.println("Good Mobile");
	}

	public void review(String text) {
		System.out.println(text);
	}

}
